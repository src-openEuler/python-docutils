%global _empty_manifest_terminate_build 0
%global modname docutils
%global _description \
Docutils is an open-source text processing system for processing plaintext\
documentation into useful formats, such as HTML, LaTeX, man-pages,\
open-document or XML. It includes reStructuredText, the easy to read, easy\
to use, what-you-see-is-what-you-get plaintext markup language.

Name:		python-docutils
Version:	0.21.2
Release:	1
Summary:	Documentation Utilities Written in Python, for General- and Special-Purpose Use
License:	Public Domain and BSD-2-Clause and Python and GPL-3.0-or-later
URL:		http://docutils.sourceforge.net
Source0:	https://files.pythonhosted.org/packages/ae/ed/aefcc8cd0ba62a0560c3c18c33925362d46c6075480bfa4df87b28e169a9/docutils-0.21.2.tar.gz
BuildArch:	noarch

%description
%{_description}

%package -n python3-docutils
Summary:	Documentation Utilities Written in Python, for General- and Special-Purpose Use
Requires:   python3-lxml
Provides:	python-docutils = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:	python3-hatchling
BuildRequires:	python3-flit-core

%description -n python3-docutils
%{_description}

%package help
Summary:	Development documents and examples for docutils
Provides:	python3-docutils-doc
%description help
%{_description}

%prep
%autosetup -n docutils-%{version}

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.rst ]; then cp -af README.rst %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.md ]; then cp -af README.md %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.txt ]; then cp -af README.txt %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
touch doclist.lst
if [ -d usr/share/man ]; then
  find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/doclist.lst .

%check
export PYTHONPATH=%{buildroot}%{python3_sitelib}
python3 test/alltests.py

%files -n python3-docutils
%license COPYING.txt licenses/*
%doc FAQ.txt README.txt RELEASE-NOTES.txt
%{_bindir}/rst*
%{_bindir}/docutils
%{python3_sitelib}/docutils
%{python3_sitelib}/docutils-*.dist-info/

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Nov 25 2024 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 0.21.2-1
- Update package with version 0.21.2
  Reconcile Docutils DTD and Document Tree documentation.
  Man utf8 output uses normal "-" char(45)
  Make effect of centre-aligning figures visible in functional HTML text.
  Remove unrequired "python -m " for calling pip in venv
  Fix: flit instead setup.py and egginfo is no longer present

* Fri Sep 1 2023 luluoc <luluoc@isoftstone.com> - 0.20.1-1
- upgrade package to version 0.20.1

* Thu Jul 27 2023 zhuofeng <zhuofeng2@huawei.com> - 0.20-1
- Update package to version 0.20

* Sat Mar 4 2023 yanglongkang <yanglongkang@h-partners.com> - 0.17.1-3
- add requires python3-lxml

* Thu Jan 12 2023 yanglongkang <yanglongkang@h-partners.com> - 0.17.1-2
- Changing the binary packing mode

* Mon Dec 19 2022 wangjunqi <wangjunqi@kylinos.cn> - 0.17.1-1
- Update package to version 0.17.1

* Mon Dec 05 2022 wangjunqi <wangjunqi@kylinos.cn> - 0.19-1
- Update package to version 0.19

* Mon May 9 2022 baizhonggui <baizhonggui@h-partners.com> - 0.17.1-2
- modify license identifier

* Wed Jan 12 2022 shixuantong <shixuantong@huawei.com> - 0.17.1-1
- update version to 0.17.1

* Mon Nov 2 2020 wangjie<wangjie294@huawei.com> -0.16-5
- Type:NA
- ID:NA
- SUG:NA
- DESC:remove python2

* Thu Sep 10 2020 wangxiao <wangxiao65@huawei.com> - 0.16-4
- fix rst2odt_prepstyles error

* Mon Aug 3 2020 wutao <wutao61@huawei.com> - 0.16-3
- fix test error problem

* Wed Jul 29 2020 dingyue <dingyue5@huawei.com> - 0.16-2
- Fix 0.16-1 for error

* Thu Jul 23 2020 dingyue <dingyue5@huawei.com> - 0.16-1
- NA

* Wed Jun 24 2020 wangyue <wangyue92@huawei.com> - 0.14-8
- Fix python3.8 complied failed

* Fri Jan 10 2020 qinjian <qinjian18@huawei.com> - 0.14-7
- Update Source0

* Tue Dec 10 2019 mengxian <mengxian@huawei.com> - 0.14-6
- Package init
